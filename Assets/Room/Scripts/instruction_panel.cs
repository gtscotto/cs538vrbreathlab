﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

[System.Serializable]
public abstract class instruction_panel : MonoBehaviour
{
    public TeleportPoint teleportPoint;

   /* protected void SetTeleport()
    {
    }

    public Vector3 TeleportLocation
    {
        get { return new Vector3(teleportPoint.transform.position.x, 0f, teleportPoint.transform.position.z); }
    }

    public Quaternion TeleportRotation
    {
        get { return teleportPoint.transform.rotation; }
    }

    public float TeleportAngle
    {
        get
        {
            return teleportPoint.transform.eulerAngles.y;
        }
    }
    */
    // each derived class must set this name, it is what appears in the dropdown selection list
    private protected string stationName = "";

    public string StationName
    {
        get { return stationName; }
    }

}
