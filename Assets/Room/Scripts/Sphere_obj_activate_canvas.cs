﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR.Extras;


public class Sphere_obj_activate_canvas : MonoBehaviour
{
    public GameObject lab_canvas;


    // Start is called before the first frame update
    void Start()
    {
        lab_canvas = GameObject.FindGameObjectWithTag("Lab_station_UI");

    }

    // Update is called once per frame
    void Update()
    {

        StartCoroutine(canvas_active());
        
    }

    IEnumerator canvas_active()
    {
        // Press space to activate the canvas for 5 sec
        if (Input.GetKeyDown(KeyCode.Space))

        {
            lab_canvas.SetActive(true);
            Debug.Log("waiting ");
            yield return new WaitForSeconds(5);
            Debug.Log("done");

            //Game object will turn off
            lab_canvas.SetActive(false);

        }
    }
}
