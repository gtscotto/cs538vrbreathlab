﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakBottle : MonoBehaviour
{

    private int collisionCount = 0;
    private AudioSource asr;
    private string colliding_bottle = "";
    public GameObject destroyedVersion;
    // Start is called before the first frame update
    void Start()
    {
        asr = GetComponent<AudioSource>();
        asr.volume = 0.0f;
       
    }

    // Update is called once per frame
    void Update()
    {
        if (collisionCount > 5)
        {
            if (colliding_bottle.CompareTo("Whisky Bottle") == 0)
            {
                //asr.pitch = Mathf.Pow(1.05946f, 2);
                
                Instantiate(destroyedVersion, transform.position, transform.rotation);
                Destroy(gameObject);
            }
        }
    }


    public void BreathCollisionAction(List<ParticleCollisionEvent> collisionEvents)
    {
        collisionCount = collisionEvents.Count;
        colliding_bottle = collisionEvents[0].colliderComponent.name;
        asr.volume = 1.0f;
        asr.Play();
    }

}
