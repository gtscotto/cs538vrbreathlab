﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR.Extras;

public class LaserHandlerFeedback : MonoBehaviour
{
    public SteamVR_LaserPointer laserPointer;

    void Awake()
    {
        laserPointer.PointerIn += PointerInside;
        laserPointer.PointerOut += PointerOutside;
        laserPointer.PointerClick += PointerClick;
    }

    public void submit_form()
    {
        // Add code to submit the form data and save to CSV
        Debug.Log("<color=magenta>Submitted form data</color>");
    }

    public void PointerClick(object sender, PointerEventArgs e)
    {

        //Debug.Log("Interacting with target inside click " + e.target.name);

        if (e.target.name == "Submit Button")
        {
            submit_form();
            Destroy(GameObject.FindGameObjectWithTag("feedback_consent_canvas"));
        }

        if (e.target.name == "Close Button")
        {
            Destroy(GameObject.FindGameObjectWithTag("feedsback_consent_canvas"));
        }

        if (e.target.name == "feedback_action_handle")
        {
            GameObject action_slider = GameObject.FindGameObjectWithTag("feedback_action_slider");
            Debug.Log("<color=magenta>Clicking Slider Head</color>");
            action_slider.GetComponent<Slider>().value = 5;
        }

        if (e.target.name == "feedback_agent_handle")
        {
            GameObject agent_slider = GameObject.FindGameObjectWithTag("feedback_agent_slider");
            agent_slider.GetComponent<Slider>().value = 5;
        }
    }

    public void PointerInside(object sender, PointerEventArgs e)
    {

        //Debug.Log("Entered the Pointer Inside method");
        Debug.Log("Interacting with target " + e.target.name);
        if (e.target.name == "QButton")
        {
            Debug.Log("QButton was entered");
        }
        else if (e.target.name == "Button")
        {
            Debug.Log("Button was entered");
        }
    }

    public void PointerOutside(object sender, PointerEventArgs e)
    {

        //Debug.Log("Entered the Pointer Outside method");
        //Debug.Log("Interacting with target " + e.target.name);
        if (e.target.name == "QButton")
        {
            Debug.Log("QButton was exited");
        }
        else if (e.target.name == "Button")
        {
            Debug.Log("Button was exited");
        }
    }


}
