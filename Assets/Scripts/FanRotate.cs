﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public static class FanRotateLinqExtensions
{
    public static IEnumerable<Transform> GetTransformsByNameContains(this IEnumerable<Transform> sourceTransforms, string nameContains)
    {
        List<Transform> returnList = new List<Transform>();

        foreach (Transform t in sourceTransforms)
        {
            if (t.name.Contains(nameContains))
            {
                returnList.Add(t);
                Debug.Log("Blade Fire Names: " + t.name);
            }
        }
        return returnList;
    }
}

public class FanRotate : MonoBehaviour
{
    float xRot = 0f;
    float currentRate = 0f;
    public float rateOverride = 0f;
    int collisionCount = 0;

    // [Range(0, 0.1f)]
    public float sensitivity = .1f;

    // [Range(0, 0.1f)]
    public float decayRate = .01f;

    // [Range(0, 50f)]
    public float brakeThreshold = 10f;

    private Rigidbody rb;

    private GameObject baseFireSmokeSystem;
    private IEnumerable<ParticleSystem> bladeSmokeParticleSystems;
    private IEnumerable<Transform> bladeFireTransforms;

    Vector3 originalFanBladePos;
    Quaternion originalFanBladeRotation;
    // private Transform fanbladeOriginalTransform = new Transform();

    private AudioSource asr;


    void Awake()
    {
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < 20; i++)
        {
            frameAverageQueue.Enqueue(0f);
        }

        rb = GetComponent<Rigidbody>();

        baseFireSmokeSystem = transform.Find("BaseFireExplosion").gameObject;
        bladeSmokeParticleSystems = transform.Find("BladeSmoke").GetComponentsInChildren<ParticleSystem>();
        bladeFireTransforms = transform.Find("BladeFires").GetComponentsInChildren<Transform>().GetTransformsByNameContains("Fire Natural");

        runawayToggle.onValueChanged.AddListener(OnRunawayToggleChange);
        decayToggle.onValueChanged.AddListener(OnDecayEnableToggleChange);
        enableBladeFireToggle.onValueChanged.AddListener(OnEnableBladeFireToggleChange);

        originalFanBladePos = new Vector3(rb.position.x, rb.position.y, rb.position.z);
        originalFanBladeRotation = new Quaternion(transform.rotation.x, transform.rotation.y, transform.rotation.z, transform.rotation.w);
        // originalFanBladePos = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y, this.transform.localPosition.z);

        asr = GetComponent<AudioSource>();

    }

    private bool runaway = false;
    public Toggle runawayToggle;

    public void OnRunawayToggleChange(bool val)
    {
        runaway = val;
    }

    private bool decayOn = true;
    public Toggle decayToggle;

    public void OnDecayEnableToggleChange(bool val)
    {
        decayOn = val;
    }

    private bool enableBladeFire = false;
    public Toggle enableBladeFireToggle;

    public void OnEnableBladeFireToggleChange(bool val)
    {
        enableBladeFire = val;
    }

    private bool detach = false;

    public void OnDetachButtonPress()
    {
        detach = true;
        runaway = false;
        // runawayToggle.onValueChanged.RemoveListener(OnRunawayToggleChange);
        runawayToggle.isOn = false;
        // runawayToggle.onValueChanged.AddListener(OnRunawayToggleChange);
        baseFireSmokeSystem.SetActive(true);
    }

    public void OnResetButtonPress()
    {
        detach = false;
        rb.isKinematic = true;
        rb.useGravity = false;
        rb.position = originalFanBladePos;
        rb.rotation = originalFanBladeRotation;
        baseFireSmokeSystem.SetActive(false);
        // this.transform.localPosition = originalFanBladePos;


        // this.transform.rotation = fanbladeOriginalTransform.rotation;
        // this.transform.localScale = fanbladeOriginalTransform.localScale;
    }

    // Connect this to sensitivity slider on change dynamic
    public void OnSensitivitySliderChange(float v)
    {
        sensitivity = v;
    }

    public void OnDecayRateSLiderChange(float v)
    {
        decayRate = v;
    }

    public void OnBrakeThresholdSliderChange(float v)
    {
        brakeThreshold = v;
    }

    private Queue<float> frameAverageQueue = new Queue<float>();

    public void BreathCollisionAction(List<ParticleCollisionEvent> collisionEvents)
    {
        collisionCount = collisionEvents.Count;
        /* frameAverageQueue.Dequeue();
        frameAverageQueue.Enqueue(collisionCount * sensitivity);
        currentRate += frameAverageQueue.Average(); */

        // Debug.Log("Collision Count: " + collisionCount.ToString());
    }

    void FixedUpdate()
    {
        frameAverageQueue.Dequeue();
        frameAverageQueue.Enqueue(collisionCount * sensitivity);
        currentRate += frameAverageQueue.Average();

        if (runaway && !detach)
            currentRate += 50 * Time.deltaTime;
        else if (decayOn)
            currentRate -= decayRate * Time.deltaTime;

        // currentRate *= (1 - decayRate);

        if (currentRate < brakeThreshold && !runaway) currentRate = 0f;

        // currentRate = rateOverride;
        // currentRate = 20f; // Use to debug to five constant rotation speed

        currentRate = Mathf.Min(currentRate, 400f);
        xRot = -currentRate;

        // set X rotation to current rate
        var m_EulerAngleVelocity = new Vector3(xRot, 0, 0);
        Quaternion deltaRotation = Quaternion.Euler(m_EulerAngleVelocity * Time.deltaTime);
        // Quaternion deltaRotation = Quaternion.Euler(m_EulerAngleVelocity * Time.fixedDeltaTime);
        rb.MoveRotation(rb.rotation * deltaRotation);

        // transform.Rotate(xRot, 0f, 0f, Space.Self);

        // if (currentRate > .01f) Debug.Log("Current Rate: " + currentRate.ToString());

        float baseRate;
        if (enableBladeFire)
            baseRate = Mathf.Max(0, currentRate - 300f);
        else
            baseRate = 0f;

        foreach (ParticleSystem ps in bladeSmokeParticleSystems)
        {
            var em = ps.emission;
            // em.rateOverTime = Mathf.Max(0, currentRate - 300f);
            em.rateOverTime = baseRate;
        }

        foreach (Transform t in bladeFireTransforms)
        {
            // var scale = new Vector3(1f, 1f, 1f) * Mathf.Max(0, currentRate - 300f) / 100f;
            var scale = new Vector3(1f, 1f, 1f) * baseRate / 100f;
            t.localScale = scale;
        }

        collisionCount = 0;
    }

    private void Update()
    {
        if (detach)
        {
            rb.isKinematic = false;
            rb.useGravity = true;
            detach = false;
            asr.Play();
        }
    }


    // Update is called once per frame
    void UpdateNotActive()
    {
        currentRate = Mathf.Min(currentRate, 250f);

        currentRate -= 1f;

        // currentRate *= (1 - decayRate);

        if (currentRate < brakeThreshold) currentRate = 0f;

        // currentRate = 190f; // Use to debug to five constant rotation speed

        xRot = currentRate * Time.deltaTime;

        transform.Rotate(xRot, 0f, 0f, Space.Self);

        if (currentRate > .01f) Debug.Log("Current Rate: " + currentRate.ToString());
    }


    // Update is called once per frame
    void UpdateOld()
    {
        currentRate = Mathf.Min(currentRate, 150f);
        currentRate *= (1 - decayRate);
        /* if (Input.GetKey(KeyCode.T))
        {
            currentRate += increaseRate;
        } */
        if (currentRate < 10f) currentRate = 0f;

        currentRate = 190f; // Use to debug to five constant rotation speed

        xRot += currentRate / 400f * Time.deltaTime;

        // if (xRot > 360f) xRot -= 360f;
        // Vector3 newAngles = new Vector3(xRot, 0f, 0f);
        // transform.localEulerAngles = newAngles;

        transform.Rotate(xRot, 0f, 0f, Space.Self);

        // Debug.Log(" Angles: " + transform.localEulerAngles.ToString() + " New Angle: " + newAngles.ToString());

        if (currentRate > .01f) Debug.Log("Current Rate: " + currentRate.ToString());
    }
}