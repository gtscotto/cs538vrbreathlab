﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DataCollection : MonoBehaviour {

    //public static GameManager instance;
    public GameObject player;
    public Anemometer sensor;
    private string id;
    private bool flag = false;
    private int nextUpdate = 1;
    public string filename;

    public GameObject recordPanel;
    Button[] buttons;


   [System.Serializable]
    public struct CharacterData {
        public string id;
        public string position;
        public string rotation;
    }

    private void Start()
    {
        CSVManager.reportFileName = filename;
        buttons = recordPanel.GetComponentsInChildren<Button>();
        buttons[1].GetComponent<Image>().color = Color.black;
    }

    public void StartStopRecording()
    {
        flag = !flag;
        
        buttons[0].GetComponentInChildren<Text>().text = flag ? "Stop Recording" : "Start Recording";
        buttons[1].GetComponent<Image>().color = flag ? Color.red : Color.black;
        id = recordPanel.GetComponentInChildren<InputField>().text;
    
    }   

    void Update() {
        if (Input.GetKeyDown(KeyCode.R))
        {
            flag = true;
            Debug.Log("<color=magenta>Saving Data</color>");
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            flag = false;
            Debug.Log("<color=magenta>Stop Saving data</color>");
        }
        
            
        
        if((Time.time >= nextUpdate) && (flag == true)){
            nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            CSVManager.AppendToReport(GetReportLine());
            
        }

    }
    
    string[] GetReportLine() {
        string[] returnable = new string[4];
        returnable[0] = id;
        returnable[1] = player.transform.position.ToString();
        returnable[2] = player.transform.rotation.ToString();
        returnable[3] = sensor.ArtificalSpeed.ToString();
        return returnable;
    }

}
