﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class AnemometerPanel : MonoBehaviour
{
    public Anemometer anemometer;
    public Button backgroundButton;
    public Button foregroundButton;

    private float origLeft;
    private float origWidth;

    RectTransform foregroundRectTransform;
    RectTransform backgroundRectTransform;

    Text buttonText;

        // Start is called before the first frame update
    void Start()
    {
        foregroundRectTransform = foregroundButton.GetComponent<RectTransform>();
        origLeft = foregroundRectTransform.anchoredPosition.x - foregroundRectTransform.sizeDelta.x/2f;
        backgroundRectTransform = backgroundButton.GetComponent<RectTransform>();
        origWidth = backgroundRectTransform.sizeDelta.x;
        buttonText = backgroundButton.GetComponentInChildren<Text>();

        // Set breath visibility on start from Inspector Anemometer
        Toggle visToggle = GetComponentsInChildren<Toggle>().Where(x => x.name == "EnableBreathVisibilityToggle").First();
        visToggle.isOn = anemometer.makeBreathVisibleOnStart;

        // Set simulator on start from Inspector Anenometer
        Toggle simulatorToggle = GetComponentsInChildren<Toggle>().Where(x => x.name == "EnableBreathSimulatorToggle").First();
        simulatorToggle.isOn = anemometer.useSimulatorOnStart;

        // Debug.Log("AnemoterPanel Start");
    }

    float level = 0f;
    float increment = 3f;

    // Update is called once per frame
    void Update()
    {
        level = Mathf.Max(0f, Mathf.Min(anemometer.ArtificalSpeed * 12, origWidth));
        buttonText.text = string.Format("{0:0}%",100f * level / origWidth);
        foregroundRectTransform.anchoredPosition = new Vector2(level/2f + origLeft, foregroundRectTransform.anchoredPosition.y);
        foregroundRectTransform.sizeDelta = new Vector2(level, foregroundRectTransform.sizeDelta.y);
    }
}
