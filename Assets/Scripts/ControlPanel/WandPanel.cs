﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WandPanel : ControlPanelPrototypeBase
{

    public List<GameObject> wand_particles;




    void Start()
    {

        // set starting selection to 0 item in list
        if (wand_particles.Count > 0)
            wand_particles[0].gameObject.SetActive(true);

        // populate the drop down list
        Dropdown dropdown = GetComponentInChildren<Dropdown>();
        // dropdown.options.Clear();
        Scrollbar scrollbar = GetComponentInChildren<Scrollbar>();

        // Add event listener
        dropdown.onValueChanged.AddListener(OnMenuSelectionChange);
        scrollbar.onValueChanged.AddListener(ScrollbarCallback);
        
    }



    public void OnMenuSelectionChange(int newPrototypeIndex)
    {
        BubbleParticle.instance.keyPressed = newPrototypeIndex;
    }

    public void ScrollbarCallback(float value)
    {
        BubbleParticle.instance.intencity =Mathf.Pow((value/0.5f), 2);
        Debug.Log(BubbleParticle.instance.intencity);
    }

    void Awake()
    {
        // set the teleport location and Control Panel Name
        base.SetTeleport();
        controlPanelName = "Wand";
    }



    /*

    void Update()
    {
        Debug.Log("collisionCount  " + collisionCount);
        Vector3 pos = breath_particle.transform.position;
        Vector3 dir = (this.transform.position - pos).normalized;
        //Debug.Log("dir = " + pos);
        /*
        if (Input.GetKey("1"))
        {
            keyPressed = 1;             // For Fire option
            collisionCount = 0;

        }
        else if (Input.GetKey("2"))
        {
            keyPressed = 2;             // For Bubble option
            collisionCount = 0;

        }
        else if (Input.GetKey("3"))
        {
            keyPressed = 3;             // For Ice option
            collisionCount = 0;
            frame = 0;
            playAudio = true;
            playOnce = false;

        }
        else if (Input.GetKey("4"))
        {
            keyPressed = 4;             // For Smoke option
            collisionCount = 0;
            stromIncrement = 0;
            playAudio = true;
            playOnce = false;
        }
        
        if (keyPressed == 1)
        {
            Debug.Log("fire count " + collisionCount);

            if (collisionCount > 0)
            {
                flame_outModules.rateOverTime = collisionCount * 50;
                collisionCount--;

                Quaternion r = Quaternion.LookRotation(dir, Vector3.up);
                flame.transform.rotation = r;
            }
            else
            {
                flame_outModules.rateOverTime = 0;
            }

        }
        if (keyPressed == 0)
        {
            //Debug.Log("bubble count " + collisionCount);

            if (collisionCount > 0)
            {
                bubble_emissionModule.rateOverTime = collisionCount * 50;
                collisionCount = 0;
                ParticleSystem bubble = Bubble_Emit.GetComponent<ParticleSystem>();

                Quaternion r = Quaternion.LookRotation(dir, Vector3.up);
                Bubble_Emit.transform.rotation = r;

            }
            else
            {
                bubble_emissionModule.rateOverTime = collisionCount;
            }

        }
        if (keyPressed == 2)
        {
            Debug.Log("ice count " + collisionCount);

            if (collisionCount > 0)
            {
                ice_emissionModule.rateOverTime = collisionCount * 50;
                collisionCount = 0;
                Flare_Ice.SetActive(true);


                FrostEffect.instance.FrostAmount = frame * 0.02f;
                // Debug.Log("Frost " + FrostEffect.instance.FrostAmount);
                frame++;
                playOnce = true;

                Quaternion r = Quaternion.LookRotation(dir, Vector3.up);
                transform.Find("IceRaySet").gameObject.transform.rotation = r;

            }
            else
            {
                ice_emissionModule.rateOverTime = 0;
                Flare_Ice.SetActive(false);
                if (frame > 0)
                {
                    frame--;
                }
                else
                {
                    FrostSound.Stop();
                    playAudio = true;
                    playOnce = false;
                }
                FrostEffect.instance.FrostAmount = frame * 0.005f;
            }

            if (playAudio && playOnce)
            {
                FrostSound.Play();
                playAudio = false;
            }

        }
        if (keyPressed == 3)
        {

            //Debug.Log("collision count " + collisionCount);
            if (collisionCount > 0)
            {
                smoke_emissionModule.rateOverTime = collisionCount * 0.5f;
                stromIncrement += 0.15f;
                storm_emissionModule.rateOverTime = stromIncrement;
                collisionCount--;
                stormSound.volume = stromIncrement / 10; //adjust this
                playOnce = true;
                Quaternion r = Quaternion.LookRotation(dir, Vector3.up);
                smoke.transform.rotation = r;
            }
            else
            {
                //Debug.Log("collision count 0");

                smoke_emissionModule.rateOverTime = 0;
                if (stromIncrement > 0)
                {
                    stromIncrement -= 0.2f;
                    storm_emissionModule.rateOverTime = stromIncrement;
                    stormSound.volume = stromIncrement / 10;
                }
                else
                {
                    stormSound.Stop();
                    playAudio = true;
                    playOnce = false;
                }
            }

            if (playAudio && playOnce)
            {
                //Debug.Log("sound play");
                stormSound.Play();
                playAudio = false;
            }



        }

    }



    


    public void BreathCollisionAction(List<ParticleCollisionEvent> collisionEvents)
    {
        collisionCount = collisionEvents.Count;

        Debug.Log("MESSAGE RECEIVED "+ collisionCount);

    }

    */
}
