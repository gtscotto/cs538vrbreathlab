﻿using System;
using System.Data;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.Extras;


public class CollectSurvey : MonoBehaviour
{
    public ControlPanelMain controlPanel;
    public List<GameObject> prototype_tables;
    public GameObject surveyForm;
    private Vector3 teleportPosition;
    private List<List<string>> questions = new List<List<string>>();
    private List<List<List<string>>> answers = new List<List<List<string>>>();
    public GameObject rightHand;
    // Start is called before the first frame update
    private void readXML()
    {
        string feedback_file_path = Application.dataPath + @"\feedback_questions.xml";
        foreach (XElement level1Element in XElement.Load(feedback_file_path).Elements())
        {
            List<string> questions_list = new List<string>();
            List<List<string>> answers_list = new List<List<string>>();

            foreach (XElement level2Element in level1Element.Element("questions").Elements("question"))
            {
                questions_list.Add(level2Element.Element("text").Value);
                List<string> answer = new List<string>();
                foreach (XElement level3Element in level2Element.Element("answers").Elements("answer"))
                {
                    answer.Add(level3Element.Value);
                }
                answers_list.Add(answer);

            }

            questions.Add(questions_list);
            answers.Add(answers_list);
        }
    }

    void Start()
    {
        readXML();
    }

    public void PresentSurvey()
    {
        int currentIndex = controlPanel.ActiveIndex;
        SteamVR_LaserPointer sp = rightHand.GetComponent<SteamVR_LaserPointer>();
        sp.thickness = 0.002f;

        if (currentIndex != 0)
        {
            GameObject[] placeholders = GameObject.FindGameObjectsWithTag("feedback_question_placeholder");
            GameObject[] radio = GameObject.FindGameObjectsWithTag("feedback_answer_radio");
            List<string> prototype_questions = questions[currentIndex - 1];
            for (int i = 0; i < placeholders.Length; i++)
            {
                placeholders[i].GetComponent<UnityEngine.UI.Text>().text = prototype_questions[i];
            }

            int radio_index = 0;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < answers[currentIndex - 1][i].Capacity; j++)
                {
                    radio[radio_index].transform.GetChild(1).gameObject.GetComponent<UnityEngine.UI.Text>().text = answers[currentIndex - 1][i][j];
                    radio_index++;
                }

            }

            surveyForm.GetComponent<Canvas>().enabled = true;
            Vector3 tableLocation = prototype_tables[currentIndex - 1].transform.position;
            surveyForm.transform.position = new Vector3(tableLocation.x, tableLocation.y + 2.23f, tableLocation.z);
            surveyForm.transform.rotation = controlPanel.prototypes[currentIndex].TeleportRotation;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
