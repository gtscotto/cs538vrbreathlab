﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR;
using Valve.VR.InteractionSystem;
using Valve.VR.Extras;
public class ControlPanelMain : MonoBehaviour
{
    // List of prototypes
    public List<ControlPanelPrototypeBase> prototypes;
    public List<instruction_panel> stations;


    private int activePrototypeIndex = 0;

    // for teleport fadein/fadeout
    private float fadeTime = 2f;
    private bool inFadeOut = false;
    private bool inFadeIn = false;
    private bool teleportNow = false;
    private bool recordNow = false;
    public GameObject player;
    public GameObject lab_canvas;

    // public GameObject cameraRotationContainer;
    // public GameObject vRCamera;
    private AudioSource teleportASR;

    public Player playerObject;
    
    void Start()
    {
        // reparent and disable all prototype canvases
        foreach (ControlPanelPrototypeBase prototype in prototypes)
        {
            // Get parent canvas object for prototype
            GameObject prototypeParentCanvas = prototype.transform.parent.transform.gameObject;
            // reparent to Control Panel container
            prototype.transform.SetParent(this.transform.parent, false);
            // destroy original canvas as not needed
            Destroy(prototypeParentCanvas);
            // set prototype menu inactive
            prototype.transform.gameObject.SetActive(false);
        }

        // set starting selection to 0 item in list
        if (prototypes.Count > 0)
            prototypes[0].gameObject.SetActive(true);

        // populate the drop down list
        Dropdown dropdown = GetComponentInChildren<Dropdown>();
        dropdown.options.Clear();
        foreach (ControlPanelPrototypeBase prototype in prototypes)
        {
            dropdown.options.Add(new Dropdown.OptionData(prototype.ControlPanelName));
        }

        // Add event listener
        dropdown.onValueChanged.AddListener(OnMenuSelectionChange);

        // Get teleport audio source
        teleportASR = GetComponent<AudioSource>();

        foreach (instruction_panel station in stations)
        {
            // Get parent canvas object for prototype
            GameObject lab_Canvas = station.gameObject;
            lab_canvas.SetActive(false);

        }


    }

    public void OnMenuSelectionChange(int newPrototypeIndex)
    {
        prototypes[activePrototypeIndex].gameObject.SetActive(false);
        activePrototypeIndex = newPrototypeIndex;
        prototypes[newPrototypeIndex].gameObject.SetActive(true);
        // Debug.Log("Menu selection changed: " + index.ToString());
    }

    public int ActiveIndex
    {
        get { return activePrototypeIndex; }
    }

    public void OnTeleportButtonPress()
    {
        teleportNow = true;
        Debug.Log("Teleport Button pressed");

        GameObject.FindGameObjectWithTag("controller_right").GetComponent<SteamVR_LaserPointer>().thickness = 0.0f;
        Toggle[] t = GameObject.FindGameObjectWithTag("feedback_canvas").GetComponentsInChildren<Toggle>();
        for (int i = 0; i < t.Length; i++)
        {
            t[i].isOn = false;
        }
    }

    IEnumerator canvas_active()
    {
            Debug.Log("inside function");
        // Press space to activate the canvas for 5 sec
        

            stations[activePrototypeIndex-1].gameObject.SetActive(true);
            Debug.Log("waiting ");
            yield return new WaitForSeconds(5);
            Debug.Log("done");
            Debug.Log("activePrototypeIndex " + activePrototypeIndex);

        //Game object will turn off
           stations[activePrototypeIndex - 1].gameObject.SetActive(false);

    }

        void Update()
    {
        // If teleport then go to prototype location
        if (teleportNow && !inFadeOut && !inFadeIn/* && Input.GetKey(KeyCode.F)*/)
        {
            inFadeOut = true;
            SteamVR_Fade.Start(Color.black, 2f);
            teleportNow = false;
            StartCoroutine(canvas_active());
            Debug.Log("canvas active");
        }

            if ((inFadeOut || inFadeIn) && fadeTime > 0f)
        {
            fadeTime -= Time.deltaTime;
        }
        else if (inFadeIn)
        {
            inFadeIn = false;
            fadeTime = 2f;

        }
        else if (inFadeOut)
        {
            inFadeOut = false;
            inFadeIn = true;
            fadeTime = 2f;

            var playerOffset = playerObject.trackingOriginTransform.position - playerObject.feetPositionGuess;
            playerObject.trackingOriginTransform.position = prototypes[activePrototypeIndex].TeleportLocation + playerOffset;

            var facingAngle = prototypes[activePrototypeIndex].TeleportAngle;
            var playerRotationAngle = playerObject.trackingOriginTransform.eulerAngles.y;
            var cameraRotation = playerObject.hmdTransform.transform.rotation;
            var cameraRotationY = cameraRotation.eulerAngles.y;

            var newAngle = new Vector3(0f, facingAngle + playerRotationAngle - cameraRotationY, 0f);
            playerObject.trackingOriginTransform.eulerAngles = newAngle;

            SteamVR_Fade.Start(Color.clear, 2f);
            teleportASR.Play();
            Debug.Log("prototypes[activePrototypeIndex] " + prototypes[activePrototypeIndex]);


        }
    }

    /*
    Transform cameraRig;
    Vector3 headPosition;
    Vector3 groundPosition;
    Vector3 translateVector;

    private void Update1()
    {
        // If teleport then go to prototype location
        if (teleportNow && !inFadeOut && !inFadeIn)
        {
            SteamVR_Camera cam = SteamVR_Render.Top();

            cameraRig = cam.origin;
            headPosition = SteamVR_Render.Top().head.position;

            groundPosition = new Vector3(headPosition.x, cameraRig.position.y, headPosition.z);

            Vector3 teleportLocationLocal = new Vector3(15.2f, 0f, -4.6f);

            translateVector = teleportLocationLocal - groundPosition;

            inFadeOut = true;
            SteamVR_Fade.Start(Color.black, 2f);
            teleportNow = false;
        }

        if ((inFadeOut || inFadeIn) && fadeTime > 0f)
        {
            fadeTime -= Time.deltaTime;
        }
        else if (inFadeIn)
        {
            inFadeIn = false;
            fadeTime = 2f;
        }
        else if (inFadeOut)
        {
            inFadeOut = false;
            inFadeIn = true;
            fadeTime = 2f;

            cameraRig.position += translateVector;
            
            SteamVR_Fade.Start(Color.clear, 2f);
            teleportASR.Play();
        }
    }

    private void Update2()
    {
        // If teleport then go to prototype location
        if (teleportNow && !inFadeOut && !inFadeIn)
        {
            inFadeOut = true;
            SteamVR_Fade.Start(Color.black, 2f);
            teleportNow = false;
        }

        if ((inFadeOut || inFadeIn) && fadeTime > 0f)
        {
            fadeTime -= Time.deltaTime;
        }
        else if (inFadeIn)
        {
            inFadeIn = false;
            fadeTime = 2f;
        }
        else if (inFadeOut)
        {
            inFadeOut = false;
            inFadeIn = true;
            fadeTime = 2f;
            // player.transform.position = new Vector3(player.transform.position.x - 5f, player.transform.position.y, player.transform.position.z - 3f);
            player.transform.position = new Vector3(prototypes[activePrototypeIndex].TeleportLocation.x, 
                player.transform.position.y, prototypes[activePrototypeIndex].TeleportLocation.z);

            // cameraRotationContainer.transform.localEulerAngles = new Vector3(cameraRotationContainer.transform.localEulerAngles.x, 
            //    90f, cameraRotationContainer.transform.localEulerAngles.z);

            // float camYRotation = vRCamera.transform.eulerAngles.y; //    .rotation.y;

            // cameraRotationContainer.transform.eulerAngles = new Vector3(cameraRotationContainer.transform.eulerAngles.x,
            //    0f - camYRotation, cameraRotationContainer.transform.eulerAngles.z);

            // cameraRotationContainer.transform.rotation = Quaternion.Euler(cameraRotationContainer.transform.rotation.x,
            //    0f - camYRotation, cameraRotationContainer.transform.rotation.z);

            // player.transform.localRotation = Quaternion.Euler(player.transform.localRotation.x, 0f,
            //    player.transform.localRotation.z);

            // player.transform.localRotation = Quaternion.Euler(player.transform.localRotation.x, prototypes[activePrototypeIndex].TeleportLocation.y, 
            //    player.transform.localRotation.z);
            // player.transform.localEulerAngles = new Vector3(player.transform.localEulerAngles.x, prototypes[activePrototypeIndex].TeleportLocation.y,
            // player.transform.localEulerAngles.z);
            SteamVR_Fade.Start(Color.clear, 2f);
            teleportASR.Play();
        }
    }
    */
}
