﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandlePanel : ControlPanelPrototypeBase
{
    void Awake()
    {
        // set the teleport location and Control Panel Name
        // teleportLocation = new Vector3(-21f, 90f, 1.2f);
        base.SetTeleport();
        controlPanelName = "Candle";
    }

}
