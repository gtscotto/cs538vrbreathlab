﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindmillPanel : ControlPanelPrototypeBase
{
    void Awake()
    {
        // set the teleport location and Control Panel Name
        base.SetTeleport();
        controlPanelName = "Windmill";
    }
}
