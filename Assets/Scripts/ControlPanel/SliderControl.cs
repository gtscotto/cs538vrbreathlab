﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;

public class SliderControl : MonoBehaviour
{
    private Slider slider;
    private Text minNumText;
    private Text maxNumText;
    private InputField inputField;

    private Slider.SliderEvent sliderEvent;

    public float sliderMinValue = 0f;
    public float sliderMaxValue = 1f;
    public float sliderInitialValue = 0.5f;
    public string sliderTitleText = "Not Defined";

    // Start is called before the first frame update
    void Start()
    {
        // get subcomponents
        slider = GetComponentInChildren<Slider>();
        slider.minValue = sliderMinValue;
        slider.maxValue = sliderMaxValue;
        slider.value = sliderInitialValue;

        minNumText = GetComponentsInChildren<Text>().Where(x => x.name == "MinText").First();
        maxNumText = GetComponentsInChildren<Text>().Where(x => x.name == "MaxText").First();
        inputField = GetComponentInChildren<InputField>();

        // change the underlying objects value
        sliderEvent = slider.onValueChanged; // This will get the source objects event delegate
        sliderEvent.Invoke(slider.value);

        // Add on change listener
        slider.onValueChanged.AddListener(OnSliderChange);

        // get the slider min and max values
        // minNumText.text = slider.minValue.ToString();
        // maxNumText.text = slider.maxValue.ToString();
        minNumText.text = sliderMinValue.ToString();
        maxNumText.text = sliderMaxValue.ToString();

        // add value to textbox
        inputField.text = slider.value.ToString();

        // Add handler if text box used to update value
        // inputField.onValueChanged.AddListener(OnTextBoxChange);
        inputField.onEndEdit.AddListener(OnTextBoxChange);

        var titleTextBox = GetComponentsInChildren<Text>().Where(x => x.name == "TitleText").First();
        titleTextBox.text = sliderTitleText;
    }

    public void OnSliderChange(float v)
    {
        // this.value = v;
        // inputField.onValueChanged.RemoveListener(OnTextBoxChange);
        inputField.text = v.ToString();
        // inputField.onValueChanged.AddListener(OnTextBoxChange);
    }

    public void OnTextBoxChange(string text)
    {
        float val = slider.value;
        if(float.TryParse(text, out val))
        {
            if (val > slider.maxValue) val = slider.maxValue;
            else if (val < slider.minValue) val = slider.minValue;
            slider.value = val;
            sliderEvent.Invoke(val);
        }
        else
        {
            inputField.text = slider.value.ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
