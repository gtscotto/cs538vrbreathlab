﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottlePanel : ControlPanelPrototypeBase
{
    void Awake()
    {
        // set the teleport location and Control Panel Name
        base.SetTeleport();
        controlPanelName = "Bottles";
    }
}
