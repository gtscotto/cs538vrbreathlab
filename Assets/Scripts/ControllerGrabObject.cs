﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ControllerGrabObject : MonoBehaviour
{
    public SteamVR_Input_Sources handType;
    public SteamVR_Behaviour_Pose controllerPose;
    public SteamVR_Action_Boolean grabAction;

    private GameObject collidingObject = null;
    private GameObject objectInHand = null;

    private Vector3 savePosition;
    private Quaternion saveRotation;

    private HashSet<string> interactables = new HashSet<string>() { "Wand", "Bottle1", "Bottle2", "Whisky Bottle", "NewWindmill1201" };

    private void SetCollidingObject(Collider col)
    {
        if (collidingObject || !col.GetComponent<Rigidbody>())
        {
            return;
        }
        collidingObject = col.gameObject;
        Debug.Log("colliding object " + collidingObject.name);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (interactables.Contains(other.name) && !objectInHand)
        {
            SetCollidingObject(other);
        }
    }

    private void GrabObject()
    {

        objectInHand = collidingObject;
        Debug.Log("Object in Hand " + objectInHand.name);
        // Save the position and rotation of colliding object to restore upon release.
        savePosition = collidingObject.transform.position;
        saveRotation = collidingObject.transform.rotation;

        objectInHand.transform.SetParent(this.transform);
        objectInHand.GetComponent<Rigidbody>().isKinematic = true;
        collidingObject = null;
    }


    private void ReleaseObject()
    {
        objectInHand.transform.SetParent(null);
        objectInHand.transform.position = savePosition;
        objectInHand.transform.rotation = saveRotation;
        objectInHand = null;
    }

    // Update is called once per frame
    void Update()
    {

        if (grabAction.GetLastStateDown(handType) && !objectInHand)
        {
            if (collidingObject)
            {
                GrabObject();
            }
        }


        if (grabAction.GetLastStateUp(handType))
        {
            if (objectInHand)
            {
                ReleaseObject();
            }
        }
    }
}
