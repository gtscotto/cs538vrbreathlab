﻿/* SceneHandler.cs*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Valve.VR.Extras;
using System.IO;

public class LaserHandler : MonoBehaviour
{
    public SteamVR_LaserPointer laserPointer;
    public GameObject consent_form;
    //public GameObject feedback_form;
    public GameObject DataCollector;
    public ControlPanelMain controlPanel;
    private bool consent_provided = false;




    public void Start()
    {
        //Debug.Log("inside the write function");
        string path = @"Assets\SurveyResults.txt";
        File.Create(path);
    }

    void Awake()
    {
        laserPointer.PointerIn += PointerInside;
        laserPointer.PointerOut += PointerOutside;
        laserPointer.PointerClick += PointerClick;
    }

    private void WriteFeedback()
    {
        GameObject feedback_form = GameObject.FindGameObjectWithTag("feedback_canvas");


        string question1 = feedback_form.transform.GetChild(3).gameObject.GetComponentInChildren<UnityEngine.UI.Text>().text;
        string question2 = feedback_form.transform.GetChild(4).gameObject.GetComponentInChildren<UnityEngine.UI.Text>().text;

        GameObject answerPanel1 = feedback_form.transform.GetChild(5).gameObject;
        GameObject answerPanel2 = feedback_form.transform.GetChild(6).gameObject;



        string path = Application.dataPath + @"\SurveyResults.txt";



        TextWriter tw = new StreamWriter(path, true);
        tw.WriteLine("Feedback for " + controlPanel.prototypes[controlPanel.ActiveIndex].name);
        tw.WriteLine(question1);
        string answer1 = "";
        Toggle[] tgrp1 = answerPanel1.GetComponentsInChildren<Toggle>();
        for (int i = 0; i < tgrp1.Length; i++)
        {
            if (tgrp1[i].isOn)
            {
                answer1 = answerPanel1.transform.GetChild(i).GetChild(1).GetComponent<UnityEngine.UI.Text>().text;
            }
        }

        string answer2 = "";
        Toggle[] tgrp2 = answerPanel2.GetComponentsInChildren<Toggle>();
        for (int i = 0; i < tgrp2.Length; i++)
        {
            if (tgrp2[i].isOn)
            {
                answer2 = answerPanel2.transform.GetChild(i).GetChild(1).GetComponent<UnityEngine.UI.Text>().text;
            }
        }




        tw.WriteLine(answer1 + "\n");
        
        tw.WriteLine(question2);
        tw.WriteLine(answer2 + "\n");
        tw.WriteLine("\n");
        tw.Close();
    }

    public void PointerClick(object sender, PointerEventArgs e)
    {

        if (e.target.name == "Clipboard")
        {
            consent_form.GetComponent<Canvas>().enabled = true;
        }

        if (e.target.name == "Checkbox")
        {
            GameObject checkBox = GameObject.FindGameObjectWithTag("check_consent");
            checkBox.GetComponent<Toggle>().isOn = !checkBox.GetComponent<Toggle>().isOn;
        }

        if (e.target.name == "Submit_Button")
        {
            // The value of the agree consent will be available here to use. 
            consent_provided = GameObject.FindGameObjectWithTag("check_consent").GetComponent<Toggle>().isOn;
            GameObject.FindGameObjectWithTag("consent_canvas").GetComponent<Canvas>().enabled = false;
        }

        if (e.target.name == "Feedback_Submit_Button")
        {
            // Export the data here to a file.
            GameObject feedback_form = GameObject.FindGameObjectWithTag("feedback_canvas");


            WriteFeedback();

            feedback_form.GetComponent<Canvas>().enabled = false;
            GameObject.FindGameObjectWithTag("controller_right").GetComponent<SteamVR_LaserPointer>().thickness = 0.0f;

        }

        if (e.target.name == "Feedback_Close_Button")
        {
            Debug.Log("Inside the close button");
            GameObject.FindGameObjectWithTag("feedback_canvas").GetComponent<Canvas>().enabled = false;
            //gameObject.GetComponent<SteamVR_LaserPointer>().active = false;
            GameObject.FindGameObjectWithTag("controller_right").GetComponent<SteamVR_LaserPointer>().thickness = 0.0f;
        }

        if (e.target.name.StartsWith("Radio") == true)
        {
            e.target.GetComponent<Toggle>().isOn = !e.target.GetComponent<Toggle>().isOn;
        }
    }

    public void PointerInside(object sender, PointerEventArgs e)
    {
    }

    public void PointerOutside(object sender, PointerEventArgs e)
    {
    }


}