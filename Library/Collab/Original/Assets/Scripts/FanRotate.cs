﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanRotate : MonoBehaviour
{
    float x_rot = 0f;
    public float rotationSpeed = 100f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        x_rot += rotationSpeed * Time.deltaTime;
        if (x_rot > 360f) x_rot -= 360f;
        Vector3 newAngles = new Vector3(x_rot, 0f, 0f);
        transform.localEulerAngles = newAngles;
        // Debug.Log(" Angles: " + transform.localEulerAngles.ToString() + " New Angle: " + newAngles.ToString());
    }
}
